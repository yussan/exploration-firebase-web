# Firebase Exploration for Web by Yussan

![Firebase cover](https://firebase.google.com/images/social.png)

## Introduction
- **Build apps fast, without managing infrastructure**

  Firebase products like Analytics, Realtime Database, Messaging, and Crash Reporting let you move quickly and focus on your users.
  
- **Backed by Google, trusted by top apps**

  Firebase is built on Google infrastructure and scales automatically, for even the largest apps.
  
- **One console, with products that work together**

  Firebase products work great individually but share data and insights, so they work even better together.

## Prerequisites 

- Firebase account, if you don't have plase sign up at <a href="https://firebase.google.com" target="_blank">https://firebase.google.com</a>
- <a href="https://www.npmjs.com/package/firebase-tools" target="_blank">firebase-tools</a> (to run local Firebase app, please install as global dependency)
- Lattest version of <a href="https://www.google.com/chrome/browser/desktop/index.html" target="_blank">Google Chrome Browser</a>
- We are using <a href="https://nodejs.org/ target="_blank">NODE JS</a> to build sample code
- <a href="https://www.npmjs.com/package/http-server" target="_blank">http-server</a> (please install as global dependency). 
- <a href="https://yarnpkg.com">yarn</a> (Dependency manager like npm)
  ```
  npm install -g http-server
  ```
  or
  ```
  yarn global add http-server
  ```
  
## How to Start
You can start by step from 1.0 and so on. Each directory have `README.md`, please read first to know what's there and how to start it.

Any question, please contact <a href="https://twitter.com/xyussanx" target="_blank">yussan</a>

## CHANGELOG
Please open [CHANGELOG.md](changelog.md) file

## Contributors
- [Yusuf Akhsan H. (yussan)](https://twitter.com/yussan)