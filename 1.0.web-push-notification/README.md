# Web Push Notification using Firebase Cloud Message(FCM)

## Initialisation
### Install all deps
```
yarn install
```
### Create your Firebase app
Follow this link to create Firebase app : https://console.firebase.google.com

### Add Firebase to javascript project
- Open your Firebase app.
- Click add Firebase to your web app (choose client or NodeJS)

## Running
```
yarn start
```
And then open http://localhost:9090

## References :
- https://firebase.google.com/docs/cloud-messaging/js/client
