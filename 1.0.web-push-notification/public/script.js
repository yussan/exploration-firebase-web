// messaging
const messaging = firebase.messaging();
// request notification permission
messaging.requestPermission()
.then(function() {
  console.log('Notification permission granted.');
  // TODO(developer): Retrieve an Instance ID token for use with FCM.
	messaging.getToken()
		.then(function(currentToken){
			if(currentToken)//have current token
			{
				sendTokenToServer(currentToken)
				updateUIForPushEnabled(currentToken)
			}else
			{
				console.log('no instance id available, request permission to generate one')
				// show permission ui
				updateUIForPushPermissionRequired();
				setTokenSentToServer(false);
			}
		})
})
.catch(function(err) {
  console.log('An error occurred while retrieving token. ', err);
	showToken('Error retrieving Instance ID token. ', err);
	setTokenSentToServer(false);
});

// send token to server
function sendTokenToServer(token)
{
	console.log('send token to server : ')
}

// update UI for push enabled
function updateUIForPushEnabled(token)
{
	console.log('update UI...')
}
