const express = require('express');
const app = express();
const server = require('http').createServer(app)
const firebase = require('firebase')
const port = 9090

server.listen(port, function () {
  console.log('Server listening at port %d', port);
});

// Routing
app.use(express.static(__dirname + '/public'));

//initialize app
const config = {
  apiKey: "AIzaSyBJMAwf3CA5o8Fnci2vKivboSvszS6YLdE",
  authDomain: "web-push-notification-7c900.firebaseapp.com",
  databaseURL: "https://web-push-notification-7c900.firebaseio.com",
  projectId: "web-push-notification-7c900",
  storageBucket: "web-push-notification-7c900.appspot.com",
  messagingSenderId: "589510744699"
}
firebase.initializeApp(config)
